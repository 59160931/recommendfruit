package buu.informatics.s59160931.recommendfruit


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import buu.informatics.s59160931.recommendfruit.databinding.FragmentFruitBinding




// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//private const val ARG_PARAM1 = "param1"
//private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FruitFragment : Fragment() {

    private lateinit var binding: FragmentFruitBinding
    var number = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate<FragmentFruitBinding>(
            inflater,
            R.layout.fragment_fruit, container, false
        )

        var arrFruit: ArrayList<Fruit> = ArrayList()
        arrFruit.add(Fruit("Apple", "apple description", R.drawable.apple))
        arrFruit.add(Fruit("Asian pear", "asian pear description", R.drawable.asianpear))
        arrFruit.add(Fruit("Banana", "banana description", R.drawable.banana))
        arrFruit.add(Fruit("Cantaloupe", "cantaloupe description", R.drawable.cantaloupe))
        arrFruit.add(Fruit("Custard apple", "custard apple description", R.drawable.custardapple))
        arrFruit.add(Fruit("Dragon fruit", "dragon fruit description", R.drawable.dragonfruit))
        arrFruit.add(Fruit("Durian", "durian description", R.drawable.durian))
        arrFruit.add(Fruit("Grape", "grape description", R.drawable.grape))
        arrFruit.add(Fruit("Jackfruit", "jackfruit description", R.drawable.jackfruit))
        arrFruit.add(Fruit("Kiwi", "kiwi description", R.drawable.kiwi))
        arrFruit.add(Fruit("Longan", "longan description", R.drawable.longan))
        arrFruit.add(Fruit("Lychee", "lychee description", R.drawable.lychee))
        arrFruit.add(Fruit("Makhampom", "makhampom description", R.drawable.makhampom))
        arrFruit.add(Fruit("Mangosteen", "mangosteen description", R.drawable.mangosteen))
        arrFruit.add(Fruit("Orange", "orange description", R.drawable.orange))
        arrFruit.add(Fruit("Rambutan", "rambutan description", R.drawable.rambutan))
        arrFruit.add(Fruit("Sapodilla fruit", "sapodilla fruit description", R.drawable.sapodillafruit))
        arrFruit.add(Fruit("Strawberry", "strawberry description", R.drawable.strawberry))
        arrFruit.add(Fruit("Wa", "wa description", R.drawable.wa))
        arrFruit.add(Fruit("Wollongong", "wollongong description", R.drawable.wollongong))

        binding.listView.adapter = this.context?.let { CustomAdapterFruit(it, arrFruit) }

        binding.listView.setOnItemClickListener { parent: AdapterView<*>, view: View, position: Int, id: Long ->

            when(position){
                0 -> number = 0
                1 -> number = 1
                2 -> number = 2
                3 -> number = 3
                4 -> number = 4
                5 -> number = 5
                6 -> number = 6
                7 -> number = 7
                8 -> number = 8
                9 -> number = 9
                10 -> number = 10
                11 -> number = 11
                12 -> number = 12
                13 -> number = 13
                14 -> number = 14
                15 -> number = 15
                16 -> number = 16
                17 -> number = 17
                18 -> number = 18
                19 -> number = 19
            }

            when(position){
                in 0..19 -> findNavController().navigate(FruitFragmentDirections.actionFruitFragmentToDetailFragment(number))
            }
        }
        return binding.root
    }
}
