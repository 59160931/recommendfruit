package buu.informatics.s59160931.recommendfruit


import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import buu.informatics.s59160931.recommendfruit.databinding.FragmentRecommendFruitBinding
import kotlinx.android.synthetic.main.fragment_recommend_fruit.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//private const val ARG_PARAM1 = "param1"
//private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class RecommendFruitFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentRecommendFruitBinding>(inflater,
            R.layout.fragment_recommend_fruit, container, false)

        binding.apply {
            clickhereButton.setOnClickListener { view ->
                Toast.makeText(context, "Welcome to Recommend Fruit", Toast.LENGTH_SHORT).show()
                view.findNavController().navigate(RecommendFruitFragmentDirections.actionRecommendFruitFragmentToFruitFragment())
            }
        }
        setHasOptionsMenu(true)
        return binding.root
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.options_menu, menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!,
            view!!.findNavController()) || super.onOptionsItemSelected(item)
    }
}
