package buu.informatics.s59160931.recommendfruit

data class Fruit (var name: String, var description: String, var image: Int)