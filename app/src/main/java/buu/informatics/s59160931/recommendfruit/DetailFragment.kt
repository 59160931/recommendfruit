package buu.informatics.s59160931.recommendfruit


import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import buu.informatics.s59160931.recommendfruit.databinding.FragmentDetailBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//private const val ARG_PARAM1 = "param1"
//private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class DetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentDetailBinding>(inflater,
            R.layout.fragment_detail, container, false)

        val args = DetailFragmentArgs.fromBundle(arguments!!)
        //Toast.makeText(context, "Index: ${args.numbers}", Toast.LENGTH_SHORT).show()

        val arrName = arrayOf("Apple","Asian pear","Banana","Cantaloupe","Custard apple","Dragon fruit","Durian","Grape",
                              "Jackfruit","Kiwi","Longan","Lychee","Makhampom","Mangosteen","Orange","Rambutan","Sapodilla fruit",
                              "Strawberry","Wa","Wollongong"
                             )
        val arrDes = arrayOf("An apple is a sweet, edible fruit produced by an apple tree (Malus domestica). Apple trees are cultivated worldwide and are the most widely grown species in the genus Malus. The tree originated in Central Asia, where its wild ancestor, Malus sieversii, is still found today. Apples have been grown for thousands of years in Asia and Europe and were brought to North America by European colonists. Apples have religious and mythological significance in many cultures, including Norse, Greek and European Christian traditions. Apple trees are large if grown from seed. Generally, apple cultivars are propagated by grafting onto rootstocks, which control the size of the resulting tree. There are more than 7,500 known cultivars of apples, resulting in a range of desired characteristics. Different cultivars are bred for various tastes and use, including cooking, eating raw and cider production. Trees and fruit are prone to a number of fungal, bacterial and pest problems, which can be controlled by a number of organic and non-organic means. In 2010, the fruit's genome was sequenced as part of research on disease control and selective breeding in apple production.",
                             "Asian pear or China pear and other names, such as nashie pearlite: Nashi of South Korea, also known as Shingo, is a buffer of Rosaceae, which is native to Western China and a natural plant. A perennial aromatic tree of Sichuan Province, China, having panicles of white flowers and many colored fruits ranging from yellow to red to brown. Juicy green, thin, sweet, sweet, with a small smell, black or brown seeds.",
                             "A banana is an edible fruit – botanically a berry– produced by several kinds of large herbaceous flowering plants in the genus Musa. In some countries, bananas used for cooking may be called \"plantains\", distinguishing them from dessert bananas. The fruit is variable in size, color, and firmness, but is usually elongated and curved, with soft flesh rich in starch covered with a rind, which may be green, yellow, red, purple, or brown when ripe. The fruits grow in clusters hanging from the top of the plant. Almost all modern edible seedless (parthenocarp) bananas come from two wild species – Musa acuminata and Musa balbisiana. The scientific names of most cultivated bananas are Musa acuminata, Musa balbisiana, and Musa × paradisiaca for the hybrid Musa acuminata × M. balbisiana, depending on their genomic constitution.",
                             "The cantaloupe, rockmelon (Australia), sweet melon, or spanspek (South Africa) is a melon that is a variety of the muskmelon (Cucumis melo) species from the Cucurbitaceae family. Cantaloupes range in weight from 0.5 to 5 kilograms (1 to 11 lb). Originally, cantaloupe referred only to the non-netted, orange-fleshed melons of Europe, but today may refer to any orange-fleshed melon of the C. melo species.",
                             "Custard apple is a common name for a fruit, and the tree which bears it, Annona reticulata. The fruits vary in shape, heart-shaped, spherical, oblong or irregular. The size ranges from 7 centimetres (2.8 in) to 12 centimetres (4.7 in), depending on the cultivar. When ripe, the fruit is brown or yellowish, with red highlights and a varying degree of reticulation, depending again on the variety. The flesh varies from juicy and very aromatic to hard with a repulsive taste. The flavor is sweet and pleasant, akin to the taste of 'traditional' custard.",
                             "Dragon fruit (Hylocereus genus) belongs to the Cactaceae family, which is the same family as the cactus. Originally from Mexico, came to Asia first in Vietnam By a French bishop about 100 years ago, planted along the eastern seaboard from Trang city to Saigon.",
                             "The durian is the fruit of several tree species belonging to the genus Durio. There are 30 recognised Durio species, at least nine of which produce edible fruit, with over 300 named varieties in Indonesia, 100 in Thailand and 100 in Malaysia. Durio zibethinus is the only species available in the international market: other species are sold in their local regions. It is native to Borneo and Sumatra. Named in some regions as the \"king of fruits\", the durian is distinctive for its large size, strong odour, and thorn-covered rind. The fruit can grow as large as 30 centimetres (12 in) long and 15 centimetres (6 in) in diameter, and it typically weighs one to three kilograms (2 to 7 lb). Its shape ranges from oblong to round, the colour of its husk green to brown, and its flesh pale yellow to red, depending on the species.",
                             "The grapefruit (Citrus × paradisi) is a subtropical citrus tree known for its relatively large sour to semi-sweet, somewhat bitter fruit. Grapefruit is a citrus hybrid originating in Barbados as an accidental cross between two introduced species – sweet orange (C. sinensis), and pomelo (or shaddock) (C. maxima) – both of which were introduced from Asia in the seventeenth century. When found, it was nicknamed the \"forbidden fruit\". Frequently, it is misidentified as the very similar parent species, pomelo.The grape part of the name alludes to clusters of fruit on the tree that often appear similar to grape clusters. The interior flesh is segmented and varies in color from white to yellow to red to pink.",
                             "The jackfruit (Artocarpus heterophyllus), also known as jack tree, is a species of tree in the fig, mulberry, and breadfruit family (Moraceae). Its native range is unknown but most sources place its center of origin in the region between the Western Ghats of southern India to the rainforests of Borneo. The jack tree is well-suited to tropical lowlands, and it bears the largest fruit of all trees; reaching as much as 55 kg (120 lb) in weight, 90 cm (35 in) in length, and 50 cm (20 in) in diameter. A mature jack tree can produce about 100–200 fruits in a year. The jackfruit is a multiple fruit composed of hundreds to thousands of individual flowers, and the fleshy petals of the unripe fruit are eaten. The immature fruit (unripe, commercially labeled as young jackfruit) has a mild taste and meat-like texture that lends itself to being a meat substitute for vegetarians and vegans. The ripe fruit can be much sweeter (depending on variety) and is more often used for desserts.",
                             "Kiwifruit (often shortened to kiwi outside Australia and New Zealand), or Chinese gooseberry, is the edible berry of several species of woody vines in the genus Actinidia. The most common cultivar group of kiwifruit (Actinidia deliciosa 'Hayward') is oval, about the size of a large hen's egg (5–8 cm (2.0–3.1 in) in length and 4.5–5.5 cm (1.8–2.2 in) in diameter). It has a thin, hair-like, fibrous, sour-but-edible light brown skin and light green or golden flesh with rows of tiny, black, edible seeds. The fruit has a soft texture with a sweet and unique flavour. China produced 50% of the world total of kiwifruit in 2017.",
                             "Longan Scientific name: Dimocarpus longan (often incorrectly written as fiber). The northern folk name is \"Longan shoulder\". The English name Longan belongs to the family Sapindaceae. Tropical and sub-tropical plants. Is a medium-sized perennial plant Brown trunk Flowering is a creamy white bouquet. The spherical bouquet is The fruit is greenish brown. Ripe fruits are pure brown. White or light pink flesh Black seeds into it Grain texture.",
                             "Lychee is the sole member of the genus Litchi in the soapberry family, Sapindaceae. It is a tropical tree native to the Guangdong and Fujian provinces of southeastern China, where cultivation is documented from the 11th century. China is the main producer of lychees, followed by India, other countries in Southeast Asia, the Indian Subcontinent and South Africa. A tall evergreen tree, the lychee bears small fleshy fruits. The outside of the fruit is pink-red, roughly textured and inedible, covering sweet flesh eaten in many different dessert dishes.Lychee seeds contain methylene cyclopropyl glycine which can cause hypoglycemia associated with outbreaks of encephalopathy in undernourished Indian and Vietnamese children who had consumed lychee fruit.",
                             "Makhampom or Indian Makhampom (Scientific name: Phyllanthus emblica) is a perennial plant in the family Phyllanthaceae. Is a fruit that is high in vitamin C. And has medicinal value as well Makhampom is a tree of Sa Kaeo province. There are other native names: Kantot (Khmer - Kanchanaburi), Kam Thuat (Ratchaburi), Makhampom (general), Mang Lu, San Ya Sa (Karen - Mae Hong Son).Makhampom is considered the fruit that has the highest vitamin C. Among all fruits Because of only one small emblica Gives 12 times more vitamin C than synthetic vitamin C and 20 times more than squeezed orange juice.",
                             "Mangosteen (Garcinia mangostana), also known as the purple mangosteen, is a tropical evergreen tree with edible fruit native to Island Southeast Asia. Its exact origins are unknown due to its widespread cultivation since ancient times, but it is believed to have been somewhere between the Sunda Islands and the Moluccas. It grows mainly in Southeast Asia, southwest India and other tropical areas such as Colombia, Puerto Rico and Florida, where the tree has been introduced. The tree grows from 6 to 25 m (19.7 to 82.0 ft) tall. The fruit of the mangosteen is sweet and tangy, juicy, somewhat fibrous, with fluid-filled vesicles (like the flesh of citrus fruits), with an inedible, deep reddish-purple colored rind (exocarp) when ripe. In each fruit, the fragrant edible flesh that surrounds each seed is botanically endocarp, i.e., the inner layer of the ovary. Seeds are almond-shaped and -sized.",
                             "The orange is the fruit of the citrus species Citrus × sinensis in the family Rutaceae, native to China.  It is also called sweet orange, to distinguish it from the related Citrus × aurantium, referred to as bitter orange. The sweet orange reproduces asexually (apomixis through nucellar embryony); varieties of sweet orange arise through mutations. The orange is a hybrid between pomelo (Citrus maxima) and mandarin (Citrus reticulata). The chloroplast genome, and therefore the maternal line, is that of pomelo. The sweet orange has had its full genome sequenced.",
                             "The rambutan (/ræmˈbuːtən/, taxonomic name: Nephelium lappaceum) is a medium-sized tropical tree in the family Sapindaceae. The name also refers to the edible fruit produced by this tree. The rambutan is native to Malaysia, and other regions of tropical Southeast Asia. It is closely related to several other edible tropical fruits including the lychee, longan, and mamoncillo.",
                             "Sapodilla is a species of evergreen plant. The fruit is sweet and fragrant. It is popular as a snack and is popular in Thailand. Sapodilla fruit is ripe with high sugar. And consists of vitamin A and C, calcium, phosphorus, raw sapodilla, and white rubber like milk There is a substance called \"gutto\". Sapodilla has another native name is Java Nil (Pattani, Malay, Yala).",
                             "The garden strawberry (or simply strawberry; Fragaria × ananassa)  is a widely grown hybrid species of the genus Fragaria, collectively known as the strawberries, which are cultivated worldwide for their fruit. The fruit is widely appreciated for its characteristic aroma, bright red color, juicy texture, and sweetness. It is consumed in large quantities, either fresh or in such prepared foods as jam, juice, pies, ice cream, milkshakes, and chocolates. Artificial strawberry flavorings and aromas are also widely used in products such as candy, soap, lip gloss, perfume, and many others.",
                             "Wa (Scientific name: Syzygium cumini) is a perennial plant. Originated from India to Southeast Asia In Thailand, it is found throughout the rainforest and deciduous forest. From near the sea to an altitude of 1,100 meters, it is auspicious plant in Phetchaburi.",
                             "Wollongong is one species of Langsat which has a thick crust and little rubber. Lagsium domesticum Corrêa is a plant from the family Meliaceae. The fruit is round, bunchy, edible. The seeds are bitter.Langsat is believed to be a fruit native to the Malay archipelago, Indonesia, the Philippines and the southern region of Thailand. There are many names such as Langsat, Duku. The name \"Langsat\" or \"Langsat\" comes from the Malay language \"langsat\". , The name \"Duku\" comes from the Indonesian language \"duku\" and the name \"Longkong\" comes from the Jawi language \"Dok Kong\"."
                             )

        if(args.numbers == 0){
            binding.apply {
                fruitImage.setImageResource(R.drawable.apples)
                nameFruit.setText(arrName.get(0))
                desFruit.setText(arrDes.get(0))
            }
        }
        if(args.numbers == 1){
            binding.apply {
                fruitImage.setImageResource(R.drawable.asianpears)
                nameFruit.setText(arrName.get(1))
                desFruit.setText(arrDes.get(1))
            }
        }
        if(args.numbers == 2){
            binding.apply {
                fruitImage.setImageResource(R.drawable.bananas)
                nameFruit.setText(arrName.get(2))
                desFruit.setText(arrDes.get(2))
            }
        }
        if(args.numbers == 3){
            binding.apply {
                fruitImage.setImageResource(R.drawable.cantaloupes)
                nameFruit.setText(arrName.get(3))
                desFruit.setText(arrDes.get(3))
            }
        }
        if(args.numbers == 4){
            binding.apply {
                fruitImage.setImageResource(R.drawable.custardapples)
                nameFruit.setText(arrName.get(4))
                desFruit.setText(arrDes.get(4))
            }
        }
        if(args.numbers == 5){
            binding.apply {
                fruitImage.setImageResource(R.drawable.dragonfruits)
                nameFruit.setText(arrName.get(5))
                desFruit.setText(arrDes.get(5))
            }
        }
        if(args.numbers == 6){
            binding.apply {
                fruitImage.setImageResource(R.drawable.durians)
                nameFruit.setText(arrName.get(6))
                desFruit.setText(arrDes.get(6))
            }
        }
        if(args.numbers == 7){
            binding.apply {
                fruitImage.setImageResource(R.drawable.grapes)
                nameFruit.setText(arrName.get(7))
                desFruit.setText(arrDes.get(7))
            }
        }
        if(args.numbers == 8){
            binding.apply {
                fruitImage.setImageResource(R.drawable.jackfruits)
                nameFruit.setText(arrName.get(8))
                desFruit.setText(arrDes.get(8))
            }
        }
        if(args.numbers == 9){
            binding.apply {
                fruitImage.setImageResource(R.drawable.kiwis)
                nameFruit.setText(arrName.get(9))
                desFruit.setText(arrDes.get(9))
            }
        }
        if(args.numbers == 10){
            binding.apply {
                fruitImage.setImageResource(R.drawable.longans)
                nameFruit.setText(arrName.get(10))
                desFruit.setText(arrDes.get(10))
            }
        }
        if(args.numbers == 11){
            binding.apply {
                fruitImage.setImageResource(R.drawable.lychees)
                nameFruit.setText(arrName.get(11))
                desFruit.setText(arrDes.get(11))
            }
        }
        if(args.numbers == 12){
            binding.apply {
                fruitImage.setImageResource(R.drawable.makhampoms)
                nameFruit.setText(arrName.get(12))
                desFruit.setText(arrDes.get(12))
            }
        }
        if(args.numbers == 13){
            binding.apply {
                fruitImage.setImageResource(R.drawable.mangosteens)
                nameFruit.setText(arrName.get(13))
                desFruit.setText(arrDes.get(13))
            }
        }
        if(args.numbers == 14){
            binding.apply {
                fruitImage.setImageResource(R.drawable.oranges)
                nameFruit.setText(arrName.get(14))
                desFruit.setText(arrDes.get(14))
            }
        }
        if(args.numbers == 15){
            binding.apply {
                fruitImage.setImageResource(R.drawable.rambutans)
                nameFruit.setText(arrName.get(15))
                desFruit.setText(arrDes.get(15))
            }
        }
        if(args.numbers == 16){
            binding.apply {
                fruitImage.setImageResource(R.drawable.sapodillafruits)
                nameFruit.setText(arrName.get(16))
                desFruit.setText(arrDes.get(16))
            }
        }
        if(args.numbers == 17){
            binding.apply {
                fruitImage.setImageResource(R.drawable.strawberrys)
                nameFruit.setText(arrName.get(17))
                desFruit.setText(arrDes.get(17))
            }
        }
        if(args.numbers == 18){
            binding.apply {
                fruitImage.setImageResource(R.drawable.was)
                nameFruit.setText(arrName.get(18))
                desFruit.setText(arrDes.get(18))
            }
        }
        if(args.numbers == 19){
            binding.apply {
                fruitImage.setImageResource(R.drawable.wollongongs)
                nameFruit.setText(arrName.get(19))
                desFruit.setText(arrDes.get(19))
            }
        }

        setHasOptionsMenu(true)
        return binding.root
    }
    // Creating our Share Intent
    private fun getShareIntent() : Intent {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.setType("text/plain")
            .putExtra(Intent.EXTRA_TEXT, getString(R.string.share))
        return shareIntent
    }
    // Showing the Share Menu Item Dynamically
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.share_menu, menu)
        // check if the activity resolves
        if (null == getShareIntent().resolveActivity(activity!!.packageManager)) {
            // hide the menu item if it doesn't resolve
            menu?.findItem(R.id.share)?.setVisible(false)
        }
    }
    // Sharing from the Menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item!!.itemId) {
            R.id.share -> shareSuccess()
        }
        return super.onOptionsItemSelected(item)
    }
    private fun shareSuccess() {
        startActivity(getShareIntent())
    }

}
